package millServer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Semaphore;

import Connections.tcpConnection;
import Packets.Packet;
import Packets.PacketHeader;
import Pools.BufferManager;
import Pools.IPMessagePool;
import Pools.PartialPacketRecord;
import Processing.PacketHandlerWorker;
import Processing.WorkerAction;
import Transmission.TransmitThread;
import Utilities.InterProcessMessage;
import Utilities.ProtectedList;
import Utilities.SharedList;


public class ServerManager extends Thread {
	
	private Selector selector;

	private static final int NUM_WORKERS = 2;

	private ArrayList<tcpConnection> queuedAccepts = new ArrayList<tcpConnection>();
	public static Semaphore acceptListAccess = new Semaphore(1, true);
	
	private ArrayList<tcpConnection> queuedCloses = new ArrayList<tcpConnection>();
	public static Semaphore closeListAccess = new Semaphore(1, true);
	
	private ArrayList<tcpConnection> connectionList = new ArrayList<tcpConnection>();
	ProtectedList<tcpConnection> connectionTSList = new ProtectedList<tcpConnection>(connectionList);
	
	private Map<String, ProtectedList<tcpConnection>> connectionGroups = new HashMap<String, ProtectedList<tcpConnection>>();
	
	private int selections;
	
	private PacketHeader headerTester = new PacketHeader();
	
	private ByteBuffer rcv = ByteBuffer.allocate(3000); 
	
	//the master message list for giving tasks to the workers
	private ArrayList<InterProcessMessage> taskList = new ArrayList<InterProcessMessage>();
	private SharedList<InterProcessMessage> workerInbox = new SharedList<InterProcessMessage>(1000, taskList); //store room for all partial messages being completed at once...
	
	private ArrayList<Packet> outboxList = new ArrayList<Packet>();
	private SharedList<Packet> outbox = new SharedList<Packet>(1000, outboxList); //store room for all partial messages being completed at once...
	
	private PacketHandlerWorker[] workers = new PacketHandlerWorker[NUM_WORKERS];

	private ArrayList<WorkerAction> wIb = new ArrayList<WorkerAction>();
	private SharedList<WorkerAction> workInbox = new SharedList<WorkerAction>(1000, wIb);
	
	private InterProcessMessage message;
	
	public ServerManager()
	{
		
		try {
			/* create the initial connection group */
			ArrayList<tcpConnection> allClients = new ArrayList<tcpConnection>();
			ProtectedList<tcpConnection> theList = new ProtectedList<tcpConnection>(allClients);
			
			connectionGroups.put("ALL_REGISTERED", theList);
			
			
			//and start the transmission thread(s)
			TransmitThread th = new TransmitThread( this, connectionGroups );
			th.start();
			
			/* boot up the worker threads */
			for(int i=0;i<workers.length;++i)
			{
				workers[i] = new PacketHandlerWorker( connectionGroups, workerInbox, outbox );
				workers[i].start();
			}

			selector = Selector.open();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run()
	{
		int accepts = 0,closes = 0;
		while(true)
		{
			
			accepts = queuedAccepts.size();
			closes  = queuedCloses.size();
			if(accepts > 0)
			{
				for(int i=0;i<accepts;++i)
				{
					try {
						acceptListAccess.acquire();
						tcpConnection acceptConnection = queuedAccepts.get(0);
						acceptListAccess.release();
						
						acceptConnection.getChannel().register(selector, SelectionKey.OP_READ);
						acceptConnection.getChannel().keyFor(selector).attach(acceptConnection);
						acceptConnection.setState( tcpConnection.CONNECTED );
						
						connectionTSList.insert(acceptConnection);
						
						acceptListAccess.acquire();
						queuedAccepts.remove(0);
						acceptListAccess.release();
						
					} catch (ClosedChannelException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			if( closes > 0 )
			{
				for(int i=0;i<closes;++i)
				{
					try {
						closeListAccess.acquire();
						tcpConnection closeConnection = queuedCloses.get(0);
						closeListAccess.release();

						close(closeConnection);
						
						closeListAccess.acquire();
						queuedCloses.remove(0);
						closeListAccess.release();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			try {
				selections = selector.select(150); //any channels available to read?
				
				if(selections > 0)
				{
					Set<SelectionKey> keys = selector.selectedKeys();
					synchronized (keys)
					{
						for (Iterator<SelectionKey> iter = keys.iterator(); iter.hasNext();) 
						{
							SelectionKey selectionKey = iter.next();
							iter.remove();
							int ops = selectionKey.readyOps();
							if ((ops & SelectionKey.OP_READ) == SelectionKey.OP_READ) 
							{	
								handleRead( selectionKey );
							}
						}
						keys.notifyAll();
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	private void handleRead(SelectionKey selectionKey) {
		//assume it has valid data to read..

		int numBytes = 0;
		
		try {
			numBytes = ((SocketChannel)selectionKey.channel()).read(rcv);
		} catch (IOException e) {
			e.printStackTrace();
			close( (tcpConnection)selectionKey.attachment() );
			selectionKey.cancel();
		}
		
		if( numBytes <= 0 )
		{
			close( (tcpConnection)selectionKey.attachment() );
			selectionKey.cancel();
			return;
		}
		
		rcv.flip(); //flip the buffer for processing
		
		Packet packet = msgExtract( selectionKey, numBytes ); 
		
		rcv.position(0).limit(rcv.capacity()); //processing complete, reset the buffer 
		
		if (packet != null)
		{
			try {
				packet.setOwner((tcpConnection)selectionKey.attachment()); /* attach the connection to the packet */
				
				/* Warning: the below call to retrieve a free message from the message pool should never fail (null) */
				/* if it fails, the server crashes. The insert method should stall the task long before there aren't */
				/* enough task messages... */
				message = IPMessagePool.getMessage().setContent(WorkerAction.PROCESS_PACKET, packet);
				workerInbox.insert(message); //place the message in the inbox
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

	private Packet msgExtract(SelectionKey selectionKey, int numBytes) {
		
		PartialPacketRecord pR;
		int neededBytes;
		tcpConnection connection = (tcpConnection)selectionKey.attachment();
		connection.setLastActive(System.currentTimeMillis());
		
		//if( !connection.isStreaming() )
		//{
			pR = BufferManager.getPartialRecord( connection.getServerGeneratedIdentifier() );
			
			if( pR != null )	//has a record, this means a message is in progress from this sender
			{

				pR.append(rcv);
				
				if ( pR.buffer().position() > PacketHeader.LENGTH )
				{
					Packet.readHeader (headerTester, 0, pR.buffer() );//refresh the header information for this client
					neededBytes = headerTester.getSize()-pR.buffer().position();
					if( neededBytes < 0 )
					{	//something bad happened
						BufferManager.release( pR ); 		//release the buffer back into free pool
						return null;
					}
					pR.setNeededBytes( neededBytes );	//and refresh the partial packets needed bytes count
				}

				if( pR.neededBytes() == 0 ) //assume the packet is complete
				{
					pR.buffer().flip();
					Packet packet = Packet.parsePacket( pR.buffer(), headerTester.getType() ); //copy buffer to a packet
					BufferManager.release( pR ); 		//release the buffer back into free pool
					return packet;
				}
				//else still not enough data for a header
				
			}	
			else		//doesn't have a record, is the packet already complete?
			{
				if( numBytes > PacketHeader.LENGTH )
				{
					Packet.readHeader(headerTester, 0, rcv);
					
					if( headerTester.getKey() == PacketHeader.MESSAGE_KEY ) /* is the message valid? */
					{
						//the key is valid
						if( numBytes == headerTester.getSize() )
						{
							//the packet is complete
							return Packet.parsePacket( rcv, headerTester.getType() );
						}
						else //the packet is not complete
						{
							pR = BufferManager.make( connection.getServerGeneratedIdentifier() );
							pR.append(rcv);
							pR.setNeededBytes( headerTester.getSize()-pR.buffer().position() );
						}
					}
					//else{} the message is not valid, do nothing with it
				}
				else // save partial
				{
					pR = BufferManager.make( connection.getServerGeneratedIdentifier() );
					pR.append(rcv);
				}
			}
	    //}
		//else //the client is streaming data... TODO
		//{
			
			
			
		//}
				
		return null;
	}
	
	public void addClosedConnection(tcpConnection connection)
	{
		try {
			closeListAccess.acquire();
			queuedCloses.add(connection);
			closeListAccess.release();
			
			selector.wakeup(); //process the close
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	

	private void close(tcpConnection connection) {
		
		if( connection.getState() != tcpConnection.CLOSING ) //already closing?
		{
			connection.setState(tcpConnection.CLOSING);//disallow sends to this client

			PartialPacketRecord pR;
			
			if( (pR = BufferManager.getPartialRecord( connection.getServerGeneratedIdentifier())) != null ) /* does it have a record? */
			{
				BufferManager.release(pR);	/* yes, release it */
			}
			
			try {
				connectionTSList.remove(connection);
				connection.getChannel().close();
				
				message = IPMessagePool.getMessage().setContent(WorkerAction.UNREGISTER_CONNECTION, connection);
				workerInbox.insert(message); //place the message in the inbox
			
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	//run from the main thread
	public void addNewConnection( SocketChannel acceptSocketChannel ) throws InterruptedException
	{
		try 
		{
			acceptSocketChannel.configureBlocking(false);

		
			if( queuedAccepts.size() >= ChatServer.MAX_CLIENTS )
			{
				acceptSocketChannel.close();
			}
			else
			{
				acceptListAccess.acquire();
				queuedAccepts.add(new tcpConnection(acceptSocketChannel));
				acceptListAccess.release();
				selector.wakeup(); //add the new connecions in the other thread
			}
		
		} catch (IOException e) 
		{
			e.printStackTrace();
		}

	}

	public ProtectedList<tcpConnection> getClientList() {
		return connectionTSList;
	}

	public SharedList<Packet> getOutbox() {
		return outbox;
	}
	
}
