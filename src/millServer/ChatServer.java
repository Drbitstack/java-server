package millServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.Iterator;
import java.util.Set;

import Connections.tcpConnection;
import Pools.BufferManager;
import Pools.IPMessagePool;
import Pools.PacketPool;
import Utilities.Debugger;
import Utilities.ProtectedList;


public class ChatServer {
	
	private static ServerSocketChannel socket;
	
	private static Selector listenerSelector;
	
	private static int serverPort = 6567;
	
	private static final int CONNECTION_CHECK_PERIOD = 100; //ms
	
	public static final int MAX_CLIENTS = 300; //ms

	
	private static ProtectedList<tcpConnection> clientList;
	
	public static void main(String args[])
	{
		
		//create resources
		
		Debugger.getOrCreate().start(); //start the debugging thread
		
		BufferManager.makeManager();
		PacketPool.makePool();
		IPMessagePool.makePool();
		
		int selections = 0;
		
		try {
			listenerSelector = Selector.open();
			socket = listenerSelector.provider().openServerSocketChannel();
			socket.bind(new InetSocketAddress(serverPort));
			socket.configureBlocking(false);
			socket.register(listenerSelector,  SelectionKey.OP_ACCEPT);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ServerManager sm = new ServerManager();
		sm.start();
		
		clientList = sm.getClientList();
		
		while(true)
		{
			try {
				
					selections = listenerSelector.select(CONNECTION_CHECK_PERIOD);
					
					if( selections > 0 )
					{
						Set<SelectionKey> keys = listenerSelector.selectedKeys();
						synchronized (keys)
						{
							for (Iterator<SelectionKey> iter = keys.iterator(); iter.hasNext();) 
							{
								SelectionKey selectionKey = iter.next();
								iter.remove();
								int ops = selectionKey.readyOps();
								if ((ops & SelectionKey.OP_ACCEPT) == SelectionKey.OP_ACCEPT) 
								{
									sm.addNewConnection(socket.accept());
								}
							}
						}
					}
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
		/*
		for( all clients )
		{
			//process connects and disconnects
			
			//TODO monitor the existing channels for timeouts
			
		}*/
			
			
		}
	}	
}
