package Pools;

import java.nio.ByteBuffer;

public class PartialPacketRecord {

	private static final int MAX_FRAG_SIZE = 2000;
	
	private ByteBuffer bb = ByteBuffer.allocate(MAX_FRAG_SIZE);

	private int freepoolIndex;
	private int neededBytes;
	private long myKey;
	
	public PartialPacketRecord()
	{
		setFreepoolIndex(-1); //start with invalid index
		setNeededBytes(-1);//start with invalid numBytes
	}

	public int neededBytes() {

		return neededBytes;
	}

	public ByteBuffer buffer() {
		return bb;
	}

	public void append(ByteBuffer rcv) {
		this.bb.put(rcv);
	}

	public int getFreepoolIndex() {
		return freepoolIndex;
	}

	public void setFreepoolIndex(int freepoolIndex) {
		this.freepoolIndex = freepoolIndex;
	}

	public void setNeededBytes(int neededBytes) {
		this.neededBytes = neededBytes;
	}

	public long getMyKey() {
		return myKey;
	}

	public void setMyKey(long myKey) {
		this.myKey = myKey;
	}
	
}
