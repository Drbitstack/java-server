package Pools;

import java.nio.ByteBuffer;

import Packets.ChatPacket;
import Packets.DataPacket;
import Packets.Packet;
import Packets.RegisterPacket;

public class PacketPool {

	private static PacketPool thePacketPool;
	
	private static int num_packets = 300;
	
	private static RegisterPacket[] registerPackets = new RegisterPacket[num_packets];
	private static ChatPacket[] 	chatPackets = new ChatPacket[num_packets];
	private static DataPacket[] 	dataPackets = new DataPacket[num_packets];
	
	private static int[] freeRegisterPackets = new int[num_packets];
	private static int[] freeChatPackets = new int[num_packets];
	private static int[] freeDataPackets = new int[num_packets];


	public static void makePool() {
		if( thePacketPool == null )
		{
			thePacketPool = new PacketPool();
			
			makePackets();
		}
		
	}

	private static void makePackets() {
		for(int i=0;i<num_packets;++i)//instantiate them
		{
			
			registerPackets[i] = new RegisterPacket("","");
			chatPackets[i]     = new ChatPacket("");
			dataPackets[i]     = new DataPacket(0,0,0);
			freeRegisterPackets[i] = -1;
			freeChatPackets[i]	   = -1;
			freeDataPackets[i]	   = -1;
		}		
	}

	public static Packet getRegisterPacket(ByteBuffer buffer) {
		RegisterPacket registerPacket;
		
		//find a free chat packet
		for( int i=0;i<num_packets;i++ )
		{ 
			if( freeChatPackets[i] == -1 )
			{
				registerPacket = registerPackets[i];
				registerPacket.setPoolIndex(i);
				registerPacket.makeFromBuffer(buffer);
				return registerPacket;
			}
		}
		//no free packets, collect garbage? TODO
		return null;
	}

	public static void releaseRegisterPacket(RegisterPacket thePacket) {

		thePacket.setPassword("");
		thePacket.setUsername("");
		thePacket.validId = 0;
		freeRegisterPackets[thePacket.getPoolIndex()] = -1; //free the packet
		thePacket.reset();
	}

	public static Packet getChatPacket(ByteBuffer buffer) {
		
		ChatPacket chatPacket;
		
		//find a free chat packet
		for( int i=0;i<num_packets;i++ )
		{ 
			if( freeChatPackets[i] == -1 )
			{
				chatPacket = chatPackets[i];
				chatPacket.setPoolIndex(i);
				chatPacket.makeFromBuffer(buffer);
				return chatPacket;
			}
		}
		//no free packets, collect garbage? TODO
		return null;
	}
	

	public static void releaseChatPacket(ChatPacket thePacket) {
		
		thePacket.setMessage("");
		freeChatPackets[thePacket.getPoolIndex()] = -1; //free the packet
		thePacket.reset();
	}

	public static Packet getDataPacket(ByteBuffer buffer) {
		

		DataPacket dataPacket;
		
		//find a free chat packet
		for( int i=0;i<num_packets;i++ )
		{ 
			if( freeChatPackets[i] == -1 )
			{
				dataPacket = dataPackets[i];
				dataPacket.setPoolIndex(i);
				dataPacket.makeFromBuffer(buffer);
				return dataPacket;
			}
		}
		//no free packets, collect garbage? TODO
		return null;
	}
	
	public static void releaseDataPacket(DataPacket thePacket) {
		
		thePacket.setDescription(null);
		thePacket.setCount(0);
		thePacket.setOffset(0);
		thePacket.setTotDatasize(0);
		freeDataPackets[thePacket.getPoolIndex()] = -1; //free the packet
		thePacket.reset();
	}
	
}
