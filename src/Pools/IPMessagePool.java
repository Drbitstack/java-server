package Pools;

import java.util.concurrent.Semaphore;

import Utilities.InterProcessMessage;

public class IPMessagePool {
	
	public static final int IP_MESSAGE_POOL_SZ = 1000; //pool of 1000 interprocess messages
	
	private static InterProcessMessage messages[] = new InterProcessMessage[IP_MESSAGE_POOL_SZ];
	private static int freeMessageIndex[] = new int[IP_MESSAGE_POOL_SZ];

	private static Semaphore freeIdxLock = new Semaphore(1, true);
	
	private static IPMessagePool ipmp;
	
	private IPMessagePool()
	{
		for(int i=0;i<messages.length;++i)
		{
			messages[i] = new InterProcessMessage();
			freeMessageIndex[i] = -1; //-1 indicates free
		}
	}
	

	public static void makePool() {
		if( ipmp == null )
		{
			ipmp = new IPMessagePool();
		}		
	}

	public static InterProcessMessage getMessage() {

		InterProcessMessage message = null;
		
		try {
			freeIdxLock.acquire();
			
			for( int i=0;i<messages.length;++i )
			{
				if( freeMessageIndex[i] == -1 )
				{
					freeMessageIndex[i] = 1;
					messages[i].setFreePoolIdx(i);
					message = messages[i];
					break;
				}
			}
			freeIdxLock.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return message;
	}
	
	public static void releaseMessage( InterProcessMessage message )
	{
		try {
			freeIdxLock.acquire();
			freeMessageIndex[message.getFreePoolIdx()] = -1;
			message.reset();//clean its contents
			freeIdxLock.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
}
