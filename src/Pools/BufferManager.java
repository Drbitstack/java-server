package Pools;

import java.util.HashMap;
import java.util.Map;


public class BufferManager {
	
	private static final int NUM_PARTIAL_RECORDS = 1000; 

	private	static PartialPacketRecord packetRecords[] = new PartialPacketRecord[NUM_PARTIAL_RECORDS];
	private static int[] freePacketRecords = new int[NUM_PARTIAL_RECORDS];
	private static Map<Long, PartialPacketRecord> recordHash = new HashMap<Long, PartialPacketRecord>();

	
	static BufferManager theManager;
	
	private BufferManager()
	{
		
	}
	
	public static BufferManager getManager()
	{
		return theManager;
	}

	public static PartialPacketRecord getPartialRecord(long serverGeneratedIdentifier) 
	{
		return recordHash.get(serverGeneratedIdentifier);
	}

	public static void makeManager() {
		if( theManager == null )
		{
			theManager = new BufferManager();
			for( int i=0;i<packetRecords.length;++i )
			{
				packetRecords[i] = new PartialPacketRecord();
				freePacketRecords[i] = -1;
			}
		}
	}

	public static PartialPacketRecord make(long serverGeneratedIdentifier) {
		for( int i=0;i<NUM_PARTIAL_RECORDS;++i )
		{
			if(freePacketRecords[i] == -1)
			{
				freePacketRecords[i] = 1;//indicate used
				packetRecords[i].setFreepoolIndex(i);
				packetRecords[i].setMyKey(serverGeneratedIdentifier);
				recordHash.put(serverGeneratedIdentifier, packetRecords[i]);
				return packetRecords[i];
			}
		}
		
		return null;
	}
	

	public static void release(PartialPacketRecord pR) {
		int index = pR.getFreepoolIndex();
		
		freePacketRecords[index] = -1;             //indicate unused
		pR.setFreepoolIndex(-1); //cleanup the partial packet
		pR.setNeededBytes(-1);
		pR.buffer().position(0);
		pR.buffer().limit(pR.buffer().capacity());
		recordHash.remove(pR.getMyKey());
	}
	
	

}
