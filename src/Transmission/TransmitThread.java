package Transmission;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Map;

import millServer.ServerManager;

import Connections.tcpConnection;
import Packets.Packet;
import Utilities.ConnectionArrayList;
import Utilities.Debugger;
import Utilities.ProtectedList;
import Utilities.SharedList;

public class TransmitThread extends Thread{

	
	private Map<String, ProtectedList<tcpConnection>> connectionGroups;

	private SharedList<Packet> outbox;
	private Map<String, ArrayList<tcpConnection>> groupList;
	private ConnectionArrayList txScanList = new ConnectionArrayList();
	private ServerManager sm;
	
	private int list_iterator_idx = 0;
	
	public TransmitThread(ServerManager sm, Map<String, ProtectedList<tcpConnection>> connectionGroups  )
	{
		this.connectionGroups = connectionGroups;
		this.sm = sm;
		this.outbox = sm.getOutbox();
	}
	
	
	public void run()
	{
		
		while(true)
		{
			Packet thePacket;
			try {
				thePacket = outbox.remove();
				processTxPacket(thePacket);
				doTransmit();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}


	private void processTxPacket(Packet thePacket) {
		
		ProtectedList<tcpConnection> sendList = null;
		tcpConnection receiverConnection, senderConnection;
		int destCount = 0;
		boolean attempt;
		
		senderConnection = thePacket.getOwner();
		
		if( thePacket.getGroup().equals("REPLY") ) 
		{
			destCount = 1;
			try {
				senderConnection.getTsOutBox().insert(thePacket);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			txScanList.addNew(senderConnection);
		}
		else
		{
			try {
				sendList = connectionGroups.get(thePacket.getGroup()); 
				if( sendList != null )
				{
					sendList.getListProtector().acquire();   //lock this group's list while it's copied
					txScanList.addNew(sendList.getTheList());
					sendList.getListProtector().release();
					
					destCount = txScanList.size();
					
					for( int i=0;i<destCount;++i)
					{
						receiverConnection = txScanList.get(i);
						if( receiverConnection.getState() == tcpConnection.CONNECTED ) //check before we add, and again when we go to send
						{
							receiverConnection.getTsOutBox().insert(thePacket);
						}
					}
					
					if(!thePacket.isReply()) //is the packet intended for the sender as well?
					{					     //no
						attempt = senderConnection.getTsOutBox().remove(thePacket);//remove the packet from his queue
						if(attempt) destCount--; 
					}	
					else//make sure the sender has the packet queued for him
					{
						//if( senderConnection.getTsOutBox().peek().getHeader().   
					//TODO	txScanList.remove(senderConnection);		
					}
				}
				else
				{
					Debugger.getOrCreate().log("Malformed Packet Group: " + thePacket.getGroup() );
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		thePacket.setActiveDestCount(destCount);
	}


	private void doTransmit() {
		
		Packet thePacket;
		boolean hasWork;
		int listSize = 0;
		tcpConnection recipientConnection;
		
		do
		{
			
			//get any new packets
			if( outbox.hasItems() )
			{
				try {
					thePacket = outbox.tryRemove();
					if(thePacket != null) processTxPacket(thePacket);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			hasWork = false;
			
			listSize = txScanList.size();
			int txIdx = 0; //index helper for the array list traversal.
			boolean justRemoved = false;
			for( int i=0;i<listSize;++i ) /* for every client on the list */
			{
				recipientConnection = txScanList.get(txIdx);
				if( recipientConnection.getTsOutBox().hasItems() )
				{
					hasWork = true;
					justRemoved = doSend(recipientConnection);//if this connection is completed, return true to remove it
				}
				else
				{
					justRemoved = true;//connection didn't really have any work
				}
				
				if(justRemoved) //the send routine has indicated that this client no longer has work. The connection is closing, error, or just done for now
				{
					txScanList.remove(recipientConnection); //since we're removing someone from this txscanqueue, don't increment the index
				}
				else
				{
					txIdx++;
				}
				justRemoved = false;
			}		
		}while( hasWork );
	}
	
	private boolean doSend( tcpConnection recipientConnection )
	{
		
		
		Packet thePacket;
		ByteBuffer transmitBuffer;
		int bytesWritten = 0;
		SocketChannel channel;
		int bufferPosition;
		boolean justRemoved = false;
		channel = (SocketChannel)recipientConnection.getChannel();
		
		
		if( recipientConnection.getState() == tcpConnection.CONNECTED && channel.isOpen() )
		{
			
			thePacket = recipientConnection.getTsOutBox().peek();

			if( (System.currentTimeMillis() - thePacket.getCreationTime()) < Packet.PACKET_LIFETIME_MS ) //has the packet already decayed?
			{
				
				//tcpConnection senderConnection = thePacket.getOwner();
				
				bufferPosition = recipientConnection.getTxState().getBufferPosition();
				transmitBuffer = thePacket.getTxBuffer(); 
				transmitBuffer.position( bufferPosition ); //start from last position

				try {
					if ((bytesWritten = channel.write(transmitBuffer)) == 0);
				} catch (IOException e) {
					//e.printStackTrace();
					sm.addClosedConnection( recipientConnection );
				}
				
				if( bytesWritten > 0 )
				{
					if( (bytesWritten + bufferPosition) == transmitBuffer.limit() )
					{
						try {
							recipientConnection.getTsOutBox().remove(thePacket);
						} catch (InterruptedException e) {
							e.printStackTrace();
						} 
						recipientConnection.getTxState().clear();
						thePacket.removeDest();
					}
					else
					{
						recipientConnection.getTxState().setBufferPosition(bufferPosition+bytesWritten);
					}
					//try to send this packet again
				}
				else
				{
					recipientConnection.getTxState().logErr();
					if( recipientConnection.getTxState().getSendErrs() > tcpConnection.MAX_TRANSMIT_FAILURES )
					{
						justRemoved = true;
						sm.addClosedConnection( recipientConnection );//it's misbehaving, disconnect this client
					}
				}

			}
			else
			{
				try {
					recipientConnection.getTsOutBox().remove(thePacket);//remove the packet from my list
					recipientConnection.getTxState().clear();
					thePacket.removeDest();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
			}
		}
		else
		{
			justRemoved = true;
			sm.addClosedConnection( recipientConnection );
		}
		if( recipientConnection.getTsOutBox().isEmpty() && !justRemoved ) justRemoved = true; //no more work, remove this connection from scanlist
		return justRemoved;
		
	}
	
	
	
}
