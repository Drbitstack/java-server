package Processing;

import java.util.Map;

import Connections.tcpConnection;
import Packets.ChatPacket;
import Packets.DataPacket;
import Packets.Packet;
import Packets.RegisterPacket;
import Pools.IPMessagePool;
import Utilities.InterProcessMessage;
import Utilities.ProtectedList;
import Utilities.SharedList;

public class PacketHandlerWorker extends Thread {

	private SharedList<InterProcessMessage> workInbox;
	private SharedList<Packet> outbox;
	private Map<String, ProtectedList<tcpConnection>> connectionGroups;
	

	public PacketHandlerWorker(Map<String, ProtectedList<tcpConnection>> connectionGroups, 
			SharedList<InterProcessMessage> workInbox, SharedList<Packet> outbox) 
	{
		this.connectionGroups = connectionGroups;
		this.workInbox = workInbox;
		this.outbox = outbox;
	}

	public void run()
	{
	
		InterProcessMessage message;
		
		while( true )
		{

			try {
				message = workInbox.remove();
				processMessage(message);
			} catch (InterruptedException e) {
			}
		}
		
	}

	private void processMessage(InterProcessMessage message) {

		switch( message.getMessageType() )
		{
			case WorkerAction.PROCESS_PACKET:
				handlePacket( (Packet) message.getPayload() );
				break;
			case WorkerAction.REGISTER_CONNECTION:
				registerConnection( (tcpConnection)message.getPayload() );
				break;	
			case WorkerAction.UNREGISTER_CONNECTION:
				unregisterConnection( (tcpConnection)message.getPayload() );
				break;
		}
		
		IPMessagePool.releaseMessage(message);
	}

	private void handlePacket(Packet thePacket) {
		
		//use the packets self proclaimed type to send to handle it
		switch(Packet.lookupType(thePacket))
		{
			case REGISTER_PACKET:
				processRegister((RegisterPacket)thePacket);
				break;
			case CHAT_PACKET:
				processChat((ChatPacket)thePacket);
				break;
			case DATA_PACKET:
				processData( (DataPacket) thePacket );
				break;
			default:  //this cannot happen.
				break;
		}
		
	}

	private void processData(DataPacket thePacket) {
		
		serverEcho( thePacket, "ALL_REGISTERED", false );
				
	}

	private void processChat(ChatPacket thePacket) {
		
		serverEcho( thePacket, "ALL_REGISTERED", false );
		
	}


	private void processRegister(RegisterPacket thePacket) {
		
		try {
			
			registerConnection( thePacket.getOwner() );
			thePacket.setGroup("REPLY"); //for direct reply
			thePacket.setReply(true);
			
			thePacket.setValidId(1); //validate the client's username
			
			/* The packet originated from the packet pools, so its buffer is already reset */
			thePacket.pack(thePacket.getTxBuffer());
			thePacket.getTxBuffer().flip();//ready the buffer for first tx;
			
			outbox.insert(thePacket);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	//add this client to the connection group main list, and add the group name to the clients allowed groups
	public void registerConnection( tcpConnection owner ) {
	
		boolean foundRegistered = false;
		int groupsSize=0;
		
		ProtectedList<tcpConnection> allList = connectionGroups.get("ALL_REGISTERED");
		try {
			
			allList.insertNew(owner);

			ProtectedList<String> clientGroups = owner.getMySafeGroups();
			
			for(int i=0;i<groupsSize;++i)
			{	
				if( clientGroups.getTheList().get(i).equals("UNREGISTERED") )
				{
					clientGroups.remove(i);
					groupsSize--;
				}
				else if( clientGroups.getTheList().get(i).equals("ALL_REGISTERED") )
				{
					foundRegistered = true; //already registered
				}
			}		
			
			if( !foundRegistered ) clientGroups.insert("ALL_REGISTERED");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void unregisterConnection( tcpConnection connection ) {

		
		removeClientFromAllGroups(connection);
		
		try {
			connection.getMySafeGroups().getListProtector().acquire();
			connection.getMySafeGroups().setTheList(null); //remove the lists from the client
			connection.getMySafeGroups().getListProtector().release();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private int removeClientFromGroup( tcpConnection theClient, String theGroup )
	{
		
		//remove the client connection from the group list, and also remove the group list string from the client
		
		boolean wasRemoved = false;
		int rm_cnt = 0;
		
		ProtectedList<tcpConnection> groupList;
		
		groupList = connectionGroups.get(theGroup);
		
		if( groupList != null ) //if this group exists
		{
			try {
				//TODO, for safety...remove all instances of this client from this group
				do
				{
					wasRemoved = groupList.remove(theClient); //if the client is actually removed from the list
					if( wasRemoved )rm_cnt++;
				}while(wasRemoved);
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		return rm_cnt;
	}
	
	//removes all groups of the given name from a client
	private boolean removeGroupFromClient( tcpConnection theClient, String theGroup )
	{
		boolean wasRemoved = false;
		int num_groups;
		
		try {
			
			
			theClient.getMySafeGroups().getListProtector().acquire();
			
			num_groups = theClient.getMySafeGroups().getTheList().size();

			for( int i=0;i<num_groups;++i )
			{
				if( theClient.getMySafeGroups().getTheList().get(i).equals(theGroup) )
				{
					theClient.getMySafeGroups().remove(i);
					wasRemoved = true;
				}
			}
			
			theClient.getMySafeGroups().getListProtector().release();
		}catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		
		return wasRemoved;
	}
	
	//TODO, this routine has a chance to stall on semaphore even though it doesn't need it (size 0)
	private void removeClientFromAllGroups( tcpConnection connection )
	{
		int size = 0;
		
		try {
			connection.getMySafeGroups().getListProtector().acquire();
			
			size = connection.getMySafeGroups().getTheList().size();
	
			for( int i = 0; i<size ;++i )
			{
				removeClientFromGroup( connection, connection.getMySafeGroups().getTheList().get(i) );
			}
			connection.getMySafeGroups().getListProtector().release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	private void serverEcho( Packet thePacket, String theGroup, boolean reply ) {
		
		thePacket.setGroup(theGroup); //the group which all registered clients are in
		thePacket.setReply(reply);
		thePacket.pack(thePacket.getTxBuffer());
		thePacket.getTxBuffer().flip();//ready the buffer for first tx;
		
		try {
			outbox.insert(thePacket);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
