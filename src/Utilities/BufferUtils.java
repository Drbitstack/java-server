package Utilities;

import java.nio.ByteBuffer;

public class BufferUtils {

	public static void packString(String message, ByteBuffer bb) {
		int messageSize =  message.length();
		
		bb.putInt(messageSize); //encode message size
		for(int i=0;i<messageSize;++i)
		{
			bb.putChar(message.charAt(i));
		}
		
	}


	public static String unpackString(StringBuilder sb, ByteBuffer receiveByteBuffer) {
		
		sb.delete(0, sb.length());//prepare for reuse
		
		int messageSize = receiveByteBuffer.getInt();
		int bufIdx = receiveByteBuffer.position();
		for( int i=0;i<messageSize;++i )
		{
			sb.append(receiveByteBuffer.getChar(bufIdx+(i*2)));
		}
		receiveByteBuffer.position(bufIdx+messageSize*2);
		
		return sb.toString();
	}

}
