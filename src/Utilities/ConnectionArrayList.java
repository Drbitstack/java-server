package Utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Connections.tcpConnection;

public class ConnectionArrayList {
	
	private ArrayList <tcpConnection> theList = new ArrayList<tcpConnection>();
	private Map<Long, tcpConnection> theMap = new HashMap<Long, tcpConnection>();
	
	
	public ConnectionArrayList( ) 
	{

	}
	

	public boolean add( tcpConnection connection )
	{
		boolean attempt = theList.add(connection);
		if(attempt)theMap.put(connection.getServerGeneratedIdentifier(), connection);
		return attempt;
	}
	

	public boolean remove( Object connection )
	{
		boolean attempt = theList.remove((tcpConnection)connection);
		if(attempt) theMap.remove(((tcpConnection)connection).getServerGeneratedIdentifier());
		return attempt;
	}
	
	public void addNew( ArrayList<tcpConnection> list )
	{
		for( tcpConnection o : list )
		{
			addNew(o);
		}
	}

	public void addNew(tcpConnection conn) {
		if( !theMap.containsKey(conn.getServerGeneratedIdentifier()) )
		{
			add(conn);
		}
	}


	public tcpConnection get(int idx) {
		return theList.get(idx);
	}


	public int size() {
		return theList.size();
	}

}
