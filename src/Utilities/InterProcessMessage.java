package Utilities;

public class InterProcessMessage {
	
	private int freePoolIdx;
	
	private int messageType; //descriptor
	private Object payload;  //pointer payload
	public int getFreePoolIdx() {
		return freePoolIdx;
	}
	public void setFreePoolIdx(int freePoolIdx) {
		this.freePoolIdx = freePoolIdx;
	}
	public int getMessageType() {
		return messageType;
	}
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
	public Object getPayload() {
		return payload;
	}
	public void setPayload(Object payload) {
		this.payload = payload;
	}
	public InterProcessMessage setContent( int type, Object o ) {
		setMessageType(type);
		setPayload(o);
		return this;
	}
	public void reset() {
		setContent(0, null);
		setFreePoolIdx(-1);
	}

}
