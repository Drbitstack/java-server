package Utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Debugger extends Thread {

	private static Debugger debugger;
	
	private ArrayList<String> inboxList = new ArrayList<String>();
	private SharedList<String> inbox = new SharedList<String>(100, inboxList); 
	
	//name of File on which text will be appended,
    //currently file contains only one line
    //as "This data is before any text appended into file."
    String path = "/ChatServer_err.log";      
 
    //creating file object from given path
    File errFile = new File(path);

    FileWriter fileWriter;
    BufferedWriter bufferFileWriter;
	private int index = 0;
	
	private Debugger()
	{
		try {
			fileWriter = new FileWriter(errFile,true);
			bufferFileWriter = new BufferedWriter(fileWriter);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Debugger getOrCreate()
	{
		if(debugger == null)
		{
			debugger = new Debugger();
		}
		
		return debugger;
	}
	
	public boolean log(String string)
	{
		boolean ret = false;
		try {
			ret = inbox.tryInsert(string);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public void run()
	{

		while(true)
		{
				
			try {
				int i = 0;
				
				while (i<5)
				{
					String toPrint = inbox.remove();
					printDebug(toPrint);
					logMessage(toPrint);
					
				}

				Thread.sleep(150);
				
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
		
	}

	private void logMessage(String toPrint) {
		
        try{
        	
        	bufferFileWriter.append(toPrint);

	        bufferFileWriter.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void printDebug(String toPrint) {

		System.out.println(toPrint);
	}
	
}
