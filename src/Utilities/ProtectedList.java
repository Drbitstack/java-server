package Utilities;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import Connections.tcpConnection;

public class ProtectedList<T> {
	
	
	private ArrayList<T> theList;
	private Semaphore listProtector = new Semaphore(1, true);
	
	public ProtectedList( ArrayList<T> list )
	{
		this.theList = list;
	}

	public void insert( T o ) throws InterruptedException
	{
		listProtector.acquire();
		theList.add(o);
		listProtector.release();
	}
	
	public boolean tryInsert( T o ) throws InterruptedException
	{
		boolean attempt = listProtector.tryAcquire();
		
		if(attempt)
		{
			theList.add(o);
			listProtector.release();
		}
		
		return attempt;
	}
	
	public void remove(int i) throws InterruptedException
	{
		listProtector.acquire();
		theList.remove(i);
		listProtector.release();
	}
	
	public boolean remove(T o) throws InterruptedException
	{
		boolean attempt = false;
		listProtector.acquire();
		attempt = theList.remove(o);
		listProtector.release();
		
		return attempt;
	}
	
	public boolean tryRemove( T o ) throws InterruptedException
	{
		boolean attempt = listProtector.tryAcquire();
		
		if(attempt)
		{
			theList.remove(o);
			listProtector.release();
		}
		
		return attempt;
	}
	
	public boolean tryRemove( int i ) throws InterruptedException
	{
		boolean attempt = listProtector.tryAcquire();
		
		if(attempt)
		{
			theList.remove(i);
			listProtector.release();
		}
		
		return attempt;
	}
	
	
	public ArrayList<T> getTheList() {
		return theList;
	}

	public void setTheList(ArrayList<T> theList) {
		this.theList = theList;
	}

	public Semaphore getListProtector() {
		return listProtector;
	}

	public void setListProtector(Semaphore listProtector) {
		this.listProtector = listProtector;
	}

	public boolean hasItems() {
		return !theList.isEmpty();
	}

	public T peek() {
		T o = null;
		if( hasItems() )
		{
			o = theList.get(0);
		}
		return o;
	}

	public boolean isEmpty() {
		return theList.isEmpty();
	}

	//TODO fix exclusivity to tcp connections
	public void insertNew(tcpConnection o) {
		
		boolean found = false;
		try {
			listProtector.acquire();
			for( int i=0;i<theList.size();++i )
			{
				if( o.getServerGeneratedIdentifier() == ((tcpConnection)theList.get(i)).getServerGeneratedIdentifier() )
				{
					found = true;
					break;
				}
			}
			if( !found )
			{
				theList.add( (T)o );
			}
			listProtector.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
