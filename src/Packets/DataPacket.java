package Packets;

import java.nio.ByteBuffer;

import Utilities.BufferUtils;

public class DataPacket extends Packet {
	
	private static final int DATA_PACKET_HEADER_LENGTH = 24; //3x java longs
	
	private static final int DATA_MAX_DATA = Packet.MAX_SIZE - (PacketHeader.LENGTH+DATA_PACKET_HEADER_LENGTH);
	
	private ByteBuffer dataBuf = ByteBuffer.allocate(DATA_MAX_DATA);//payload buffer
	//note, actually should be 2000 - packetheader - datapacketheader
	
	private String description;
	private long count;
	private long offset;
	private long totDatasize;
	

	
	public DataPacket( long count, long offset, long totDataSize ) {
		super(PacketType.DATA_PACKET.getType());
		
		this.offset = offset;
		this.count = count;
		this.totDatasize = totDataSize;
	}
	

	
	//PACKET RECONSTRUCTION
	public DataPacket(ByteBuffer receiveByteBuffer) 
	{
		super(PacketType.DATA_PACKET.getType());
		makeFromBuffer( receiveByteBuffer );

	}

	public void makeFromBuffer(ByteBuffer receiveByteBuffer) {
		StringBuilder sb = new StringBuilder();
		this.description = BufferUtils.unpackString(sb, receiveByteBuffer);
		this.count = receiveByteBuffer.getLong();
		this.offset = receiveByteBuffer.getLong();
		this.totDatasize = receiveByteBuffer.getLong();
		this.dataBuf.put(receiveByteBuffer);//copy the rest to my internal data buffer
	}
	

	public ByteBuffer getDataBuf() {
		return dataBuf;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public long getTotDatasize() {
		return totDatasize;
	}

	public void setTotDatasize(long totDatasize) {
		this.totDatasize = totDatasize;
	}

	@Override
	public void pack(ByteBuffer bb) {
		
		int descriptionSize = description.length();
		
		//string lengths * length of java char + #strings*length encoded int size of 4
		int totalStringsSize = (descriptionSize)*2;
		int totalPrimitiveDigitSize = (1)*4 + 8*3; //1 string encoding + 3 longs
		
		int totalMessageSize = (int) (totalStringsSize + totalPrimitiveDigitSize + this.count);
		
		getHeader().setId(0);
		getHeader().setKey(PacketHeader.MESSAGE_KEY);
		getHeader().setSize(totalMessageSize + PacketHeader.LENGTH);
		getHeader().setType(getType());
		
		//and pack it
		getHeader().pack(bb);
		BufferUtils.packString(description, bb);
		bb.putLong(count);
		bb.putLong(offset);
		bb.putLong(totDatasize);
		bb.put(dataBuf);
	}
	
}
