package Packets;



import java.nio.ByteBuffer;

import Utilities.BufferUtils;

public class ChatPacket extends Packet{

	private String message;
	private String userName;
	
	public ChatPacket(String message) {
		super(PacketType.CHAT_PACKET.getType());
		this.message = message;
	}

	public ChatPacket(ByteBuffer receiveByteBuffer) {
		super(PacketType.CHAT_PACKET.getType());
		makeFromBuffer(receiveByteBuffer);
	}

	public String getMessage() {
		return message;
	}

	@Override
	public void pack(ByteBuffer bb) {
		
		int stringCount = 0;
		int intCount = 0;
		int totalStringsSize = 0;
		int totalIntSize = 0;
		int totalMessageSize = 0;
		
		
		totalStringsSize += message.length();
		stringCount++;
		totalStringsSize += userName.length();
		stringCount++;
		
		totalStringsSize = (totalStringsSize)*2 + (stringCount)*4;
		totalMessageSize = totalStringsSize + totalIntSize;
		
		getHeader().setId(0);
		getHeader().setKey(PacketHeader.MESSAGE_KEY);
		getHeader().setSize(totalMessageSize + PacketHeader.LENGTH); 
		getHeader().setType(getType());

		getHeader().pack(bb);
		BufferUtils.packString( userName, bb );
		BufferUtils.packString( message, bb );

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public void makeFromBuffer(ByteBuffer buffer) {
		StringBuilder sb = new StringBuilder();

		this.userName = BufferUtils.unpackString( sb, buffer );
		this.message  = BufferUtils.unpackString( sb, buffer );
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
