package Packets;

import java.nio.ByteBuffer;

import Utilities.BufferUtils;

public class RegisterPacket extends Packet {

	
	public String username;
	public String password;
	public int validId;

	public RegisterPacket( String username, String password ) 
	{
		this(username, password, 0);
	}
	
	public RegisterPacket(String username, String password, int validId) {
		super(PacketType.REGISTER_PACKET.getType());
		
		this.username = username;
		this.password = password;
		this.validId = validId;
	}
	
	//PACKET RECONSTRUCTION
	public RegisterPacket(ByteBuffer receiveByteBuffer) 
	{
		super(PacketType.REGISTER_PACKET.getType());
		makeFromBuffer( receiveByteBuffer );

	}

	//PACKET DECONSTRUCTION
	@Override
	public void pack(ByteBuffer bb) {
		
		int usernameSize = username.length();
		int passwordSize = password.length();
		
		//string lengths * length of java char + #strings*length encoded int size of 4
		int totalStringsSize = (usernameSize+passwordSize)*2;
		int totalIntSize = (2)*4 + 4; //2 string encodings + validId int
		
		int totalMessageSize = totalStringsSize + totalIntSize;
		
		getHeader().setId(0);
		getHeader().setKey(PacketHeader.MESSAGE_KEY);
		getHeader().setSize(totalMessageSize + PacketHeader.LENGTH);
		getHeader().setType(getType());
		
		//and pack it
		getHeader().pack(bb);
		bb.putInt(validId);
		BufferUtils.packString(username, bb);
		BufferUtils.packString(password, bb);
		
	}
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getValidId() {
		return validId;
	}

	public void setValidId(int validId) {
		this.validId = validId;
	}

	public void makeFromBuffer(ByteBuffer buffer) {
		StringBuilder sb = new StringBuilder();
		this.validId  = buffer.getInt();
		this.username = BufferUtils.unpackString( sb, buffer );
		this.password = BufferUtils.unpackString( sb, buffer );
		
	}


}
