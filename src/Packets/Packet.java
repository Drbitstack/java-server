package Packets;



import java.io.Serializable;
import java.nio.ByteBuffer;

import Connections.tcpConnection;
import Pools.PacketPool;

public abstract class Packet implements Serializable {

	private static final long serialVersionUID = 4561089399746790706L;
	public static final int MAX_SIZE = 2000;
	public static final long PACKET_LIFETIME_MS = 10000;
	
	private PacketHeader header = new PacketHeader();
	private tcpConnection connection; //TODO
	private String group; //used for routing the packet through the server. TO which group is it destined?
	private int poolIndex;
	
	private ByteBuffer txBuffer = ByteBuffer.allocate(MAX_SIZE);
	
	private int destCount;
	private long creationTime;
	private boolean isAlive;
	
	public enum PacketType {
		
		REGISTER_PACKET(01),
		CHAT_PACKET(02),
		INVALID_PACKET(-01), 
		DATA_PACKET(03);
		
		private int type;
		PacketType( int type)
		{
			this.type = type;
		}
		public int getType()
		{
			return this.type;
		}

	}
	
	public byte type;
	private boolean isReply;
	
	public Packet( int type )
	{
		this.type = (byte) type;
		setCreationTime(System.currentTimeMillis());
		destCount = 0;
		isReply = false;
	}

	public byte getType() {
		return type;
	}
	
	public static void readHeader( PacketHeader header, int start, ByteBuffer bb )
	{
		int startPosition = bb.position();//save the buffer position
		bb.position(start); //move to the beginning of the packet
		
		header.setKey(bb.getInt());
		header.setId(bb.getInt());
		header.setType(bb.getInt());
		header.setSize(bb.getInt());
		
		bb.position(startPosition);
	}
	
	public ByteBuffer getTxBuffer() {
		return txBuffer;
	}

	public String getGroup() {
		return group;
	}
	
	public void setGroup(String group) {
		this.group = group;
	}

	public static PacketType lookupType(Packet packet) 
	{
		int packetType = (int)packet.getType();
		return lookupType(packetType);
	}
	
	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public int getDestCount() {
		return destCount;
	}

	public void setDestCount(int destCount) {
		this.destCount = destCount;
	}
	
	//routine used from the a transmission task to setup a packet for tx;
	public void setActiveDestCount(int destCount) {
		this.destCount = destCount;
		if( destCount <= 0 )
		{
			Packet.releasePacketToPool(this);	
		}
	}

	public static PacketType lookupType(int packetType) {
		for( PacketType type : PacketType.values()  )
		{
            if (type.getType() == packetType) {
                return type;
            }
		}
		return PacketType.INVALID_PACKET;
	}

	public abstract void pack ( ByteBuffer bb ); //places the packet ready to send into the given buffer
	
	
	
	public static Packet parsePacket(ByteBuffer buffer, int type ) {
	
		//skip the header for creating the packet
		buffer.position( PacketHeader.LENGTH );
		Packet thePacket = null;
		switch( lookupType(type) )
		{
			case REGISTER_PACKET:
				thePacket = PacketPool.getRegisterPacket( buffer );
				break;
			case CHAT_PACKET:
				thePacket = PacketPool.getChatPacket( buffer );
				break;
			case DATA_PACKET:
				thePacket = PacketPool.getDataPacket( buffer );
				break;
			case INVALID_PACKET:
				default:
					break;
		}
		if(thePacket != null) thePacket.setCreationTime(System.currentTimeMillis());
		return thePacket;
		
	}

	public PacketHeader getHeader() {
		return header;
	}

	public void setOwner(tcpConnection connection) {
		this.connection = connection;
	}
	
	public tcpConnection getOwner() {
		return this.connection;
	}

	public int getPoolIndex() {
		return poolIndex;
	}

	public void setPoolIndex(int poolIndex) {
		this.poolIndex = poolIndex;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}

	public static void releasePacketToPool(Packet thePacket) {
		
		switch( lookupType(thePacket) )
		{
			case REGISTER_PACKET:
				 PacketPool.releaseRegisterPacket( (RegisterPacket)thePacket );
				 break;
			case CHAT_PACKET:
				PacketPool.releaseChatPacket( (ChatPacket)thePacket );
			    break;
			case INVALID_PACKET:
			default:
				break;
		}
		
	}

	public void reset() {
			setGroup("");
			setDestCount(0);
			setOwner(null);	//let the garbarge collector take this connection
			setCreationTime(0);
			setPoolIndex(-1);
			getTxBuffer().position(0).limit(getTxBuffer().capacity());
	}

	public boolean isReply() {
		return isReply;
	}

	public void setReply(boolean b) {
		this.isReply = b;	
	}

	public void removeDest() {
		this.destCount--;
		if( destCount <= 0 )
		{
			Packet.releasePacketToPool(this);	
		}
	}

}
