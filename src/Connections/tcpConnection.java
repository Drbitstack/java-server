package Connections;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

import Packets.Packet;
import Utilities.ProtectedList;


public class tcpConnection {
	
	//some states
	public static final int CONNECTED = 445346;
	public static final int CLOSING = 547223;
	public static final int MAX_TRANSMIT_FAILURES = 3;
	
	private SocketChannel socketChannel;
	private InetSocketAddress address;
	private String username;
	
	private ArrayList<String> myGroups = new ArrayList<String>();
	private ProtectedList<String> mySafeGroups = new ProtectedList<String>(myGroups);
	//editing groups are thread safe!
	
	private ArrayList<Packet> outBox = new ArrayList<Packet>();
	private ProtectedList<Packet> tsOutBox = new ProtectedList<Packet>(outBox);
	
	private int state = CONNECTED;
	private long serverGeneratedIdentifier;
	private long lastActive;
	
	private TxState txState = new TxState();

	public tcpConnection( SocketChannel socketChannel )
	{
		this.myGroups.add("UNREGISTERED");
		this.socketChannel = socketChannel;
		try {
			this.address = (InetSocketAddress) socketChannel.getRemoteAddress();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.serverGeneratedIdentifier = generateIdentifier(address.getHostString());
		txState.clear();
	}

	public SelectableChannel getChannel() {
		return socketChannel;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public ProtectedList<Packet> getTsOutBox() {
		return tsOutBox;
	}

	public void setTsOutBox(ProtectedList<Packet> tsOutBox) {
		this.tsOutBox = tsOutBox;
	}

	public TxState getTxState() {
		return txState;
	}

	public void setTxState(TxState txState) {
		this.txState = txState;
	}

	private static long generateIdentifier(String username) {
		
		double identifier = (((double) Math.random())*(double)1000000000000L);
		int sum = 0;
		for( int i =0; i<username.length();i++ )
		{
			sum+= username.charAt(i);
		}

		return (long)identifier+sum;
	}

	public long getServerGeneratedIdentifier() {
		return serverGeneratedIdentifier;
	}

	public void setServerGeneratedIdentifier(long serverGeneratedIdentifier) {
		this.serverGeneratedIdentifier = serverGeneratedIdentifier;
	}

	public synchronized int getState() {
		return state;
	}

	public synchronized void setState(int state) {
		this.state = state;
	}

	public InetSocketAddress getAddress() {
		return address;
	}

	public void setAddress(InetSocketAddress address) {
		this.address = address;
	}

	public long getLastActive() {
		return lastActive;
	}

	public void setLastActive(long lastActive) {
		this.lastActive = lastActive;
	}

	public ProtectedList<String> getMySafeGroups() {
		return mySafeGroups;
	}

	public void setMySafeGroups(ProtectedList<String> mySafeGroups) {
		this.mySafeGroups = mySafeGroups;
	}

}
