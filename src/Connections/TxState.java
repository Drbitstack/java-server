package Connections;

public class TxState {

	
	private int bufferPosition;
	private int sendErrs;
	
	
	
	public int getBufferPosition() {
		return bufferPosition;
	}
	public void setBufferPosition(int bufferPosition) {
		this.bufferPosition = bufferPosition;
	}
	public int getSendErrs() {
		return sendErrs;
	}
	public void setSendErrs(int sendErrs) {
		this.sendErrs = sendErrs;
	}
	
	public void logErr()
	{
		this.sendErrs++;
	}
	
	public void clear() {
		setBufferPosition(0);
		setSendErrs(0);
	}
	
}
